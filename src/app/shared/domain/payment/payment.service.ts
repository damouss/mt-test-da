import { Payment } from "./payment.model";

import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class PaymentService {
  apiUrl = environment.rootApi + "/payments";

  httOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(private _http: HttpClient) {}

  getPayments(): Observable<Payment[]> {
    return this._http.get<Payment[]>(this.apiUrl);
  }

  postPayment(payment): Observable<Payment> {
    return this._http.post<Payment>(
      this.apiUrl,
      JSON.stringify(payment),
      this.httOptions
    );
  }
}
