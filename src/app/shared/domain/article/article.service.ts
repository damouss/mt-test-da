import { Observable } from "rxjs";
import { Article } from "./article.model";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ArticleService {
  apiUrl = environment.rootApi + "/articles";

  httOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(private _http: HttpClient) {}

  getArticles(): Observable<Article[]> {
    return this._http.get<Article[]>(this.apiUrl);
  }
}
