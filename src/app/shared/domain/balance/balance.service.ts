import { Balance } from "./balance.model";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";

import { environment } from "src/environments/environment";

@Injectable({
  providedIn: "root"
})
export class BalanceService {
  apiUrl = environment.rootApi + "/balance";

  httOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  constructor(private _http: HttpClient) {}

  getBalance() {
    return this._http.get<Balance>(this.apiUrl);
  }
  // postUser(user): Observable<User> {
  //   return this._http.post<User>(
  //     this.apiUrl + "/",
  //     JSON.stringify(user),
  //     this.httOptions
  //   );
  //   // .pipe(
  //   //   tap(user => console.log(`add user w/ id=${user.id}`))
  //   //   // catchError(this.handleError<any>("addUser"))
  //   // );
  // }

  // getUser(id): Observable<User> {
  //   return this._http.get<User>(this.apiUrl + "/" + id);
  // }

  // putUser(id, user): Observable<User> {
  //   return this._http.put<User>(
  //     this.apiUrl + "/" + id,
  //     JSON.stringify(user),
  //     this.httOptions
  //   );
  // }
  // deleteUser(id) {
  //   return this._http.delete<User>(this.apiUrl + "/" + id, this.httOptions);
  // }
}
