// import { UserEditComponent } from "./pages/user-edit/user-edit.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PaymentComponent } from "./pages/payment/payment.component";
import { BalanceComponent } from "./pages/balance/balance.component";
import { AddpurchaseComponent } from "./pages/addpurchase/addpurchase.component";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "balance"
  },
  {
    path: "balance",
    component: BalanceComponent
  },
  {
    path: "payment",
    component: PaymentComponent
  },
  {
    path: "addpurchase",
    component: AddpurchaseComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
