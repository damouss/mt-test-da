import { Router } from "@angular/router";
import { PaymentService } from "src/app/shared/domain/payment/payment.service";
import { ArticleService } from "./../../shared/domain/article/article.service";
import { Article } from "./../../shared/domain/article/article.model";
import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-addpurchase",
  templateUrl: "./addpurchase.component.html",
  styleUrls: ["./addpurchase.component.scss"]
})
export class AddpurchaseComponent implements OnInit {
  articles: Article[];
  selectedArticle: Article;

  constructor(
    private articleService: ArticleService,
    private paymentService: PaymentService,
    public router: Router
  ) {}

  ngOnInit() {
    this.loadArticle();
  }

  loadArticle() {
    return this.articleService
      .getArticles()
      .subscribe(data => (this.articles = data));
  }

  addPayment() {
    this.paymentService
      .postPayment(this.selectedArticle)
      .subscribe((data: {}) => {
        this.router.navigate(["/payment"]);
      });
  }
}
