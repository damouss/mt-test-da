import { Component, OnInit } from "@angular/core";
import { Balance } from "src/app/shared/domain/balance/balance.model";
import { BalanceService } from "src/app/shared/domain/balance/balance.service";

@Component({
  selector: "app-balance",
  templateUrl: "./balance.component.html",
  styleUrls: ["./balance.component.scss"]
})
export class BalanceComponent implements OnInit {
  balance: Balance;

  constructor(private balanceService: BalanceService) {}

  ngOnInit() {
    this.loadBalance();
  }

  loadBalance() {
    return this.balanceService
      .getBalance()
      .subscribe(data => (this.balance = data));
  }
}
