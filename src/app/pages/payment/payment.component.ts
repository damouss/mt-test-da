import { MatTableDataSource } from "@angular/material/table";
import { ArticleService } from "./../../shared/domain/article/article.service";
import { Article } from "./../../shared/domain/article/article.model";
import { Payment } from "./../../shared/domain/payment/payment.model";
import { Component, OnInit } from "@angular/core";
import { PaymentService } from "src/app/shared/domain/payment/payment.service";

import { Observable, combineLatest } from "rxjs";

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"]
})
export class PaymentComponent implements OnInit {
  detailedPayments: DetailedPayment[];

  displayedColumns: string[] = [
    "article.label",
    "article.price",
    "payment.payment_date",
    "payment.state"
  ];
  // dataSource = new MatTableDataSource(this.detailedPayments);

  // applyFilter(filterValue: string) {
  //   this.dataSource.filter = filterValue.trim().toLowerCase();
  //   console.log(this.detailedPayments);
  // }

  constructor(
    private paymentService: PaymentService,
    private articleService: ArticleService
  ) {}

  ngOnInit() {
    combineLatest(this.loadPayments(), this.loadArticles()).subscribe(
      ([payments, articles]) => this.initDetailedPayments(payments, articles)
    );
  }

  loadPayments(): Observable<Payment[]> {
    return this.paymentService.getPayments();
  }

  loadArticles(): Observable<Article[]> {
    return this.articleService.getArticles();
  }

  initDetailedPayments(payments: Payment[], articles: Article[]) {
    const detailedPayments = [];
    for (const payment of payments) {
      const article = articles.find(
        article => payment.article_id === article.id
      );
      const detailedPayment = new DetailedPayment(payment, article);
      detailedPayments.push(detailedPayment);
    }
    this.detailedPayments = detailedPayments;
  }
}

class DetailedPayment {
  payment: Payment;
  article: Article;

  constructor(payment: Payment, article: Article) {
    this.payment = payment;
    this.article = article;
  }
}
