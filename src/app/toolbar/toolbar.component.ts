import { Balance } from "./../shared/domain/balance/balance.model";
import { BalanceService } from "src/app/shared/domain/balance/balance.service";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-toolbar",
  templateUrl: "./toolbar.component.html",
  styleUrls: ["./toolbar.component.scss"]
})
export class ToolbarComponent implements OnInit {
  balance: Balance;

  constructor(private balanceService: BalanceService) {}

  ngOnInit() {
    this.loadBalance();
  }

  loadBalance() {
    return this.balanceService
      .getBalance()
      .subscribe(data => (this.balance = data));
  }
}
