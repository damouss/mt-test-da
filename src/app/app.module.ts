import { ArticleService } from "./shared/domain/article/article.service";
import { PaymentService } from "./shared/domain/payment/payment.service";
import { BalanceService } from "./shared/domain/balance/balance.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatTableModule } from "@angular/material/table";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { ToolbarComponent } from "./toolbar/toolbar.component";
import { BalanceComponent } from "./pages/balance/balance.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { PaymentComponent } from "./pages/payment/payment.component";
import { MatSelectModule } from "@angular/material/select";
import { MatGridListModule } from "@angular/material/grid-list";

import { AddpurchaseComponent } from "./pages/addpurchase/addpurchase.component";

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    BalanceComponent,
    PaymentComponent,
    AddpurchaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    HttpClientModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatSelectModule,
    MatGridListModule
  ],
  providers: [BalanceService, PaymentService, ArticleService],
  bootstrap: [AppComponent]
})
export class AppModule {}
